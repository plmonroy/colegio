/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.logic;

import com.runt.colegio.dao.ProfesorDAO;
import com.runt.colegio.dto.MessageDTO;
import com.runt.colegio.dto.ProfesorDTO;
import com.runt.colegio.entities.Profesor;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro
 */
@Stateless
public class ConsultarProfesor {
    
    @EJB
    private ProfesorDAO dao;
    
    public Response consultarProfesor(ProfesorDTO inDTO){
        MessageDTO msnDTO =new MessageDTO();
        msnDTO.setCode("ERROR");
        List<Profesor> listProfesor;
        if(inDTO.isConsultaTodo()){
            listProfesor = dao.consultaTodos();
            if(listProfesor != null && !listProfesor.isEmpty()){
                List<ProfesorDTO> listProfesorDTO = new ArrayList<>();
                for (Profesor p : listProfesor) {
                    ProfesorDTO outDTO = new ProfesorDTO();
                    outDTO.setConsultaTodo(null);
                    outDTO.setNombre(p.getNombre());
                    outDTO.setListAsignaturas(p.getListAsignaturas());
                    listProfesorDTO.add(outDTO);
                }
                msnDTO.setCode("OK");       
                msnDTO.setObject(listProfesorDTO);
        }
        }else {
            if(inDTO.getNombre()!=null){
                Profesor dataProfesor = dao.consultaByID(inDTO.getNombre());
                if (dataProfesor!=null){
                    ProfesorDTO outDTO = new ProfesorDTO();
                    outDTO.setConsultaTodo(null);                    
                    outDTO.setNombre(dataProfesor.getNombre());
                    outDTO.setListAsignaturas(dataProfesor.getListAsignaturas());
                    msnDTO.setCode("OK");
                    msnDTO.setObject(outDTO);
                }
            }
            
        }

        return Response.ok(msnDTO).build();
    }
    
}
