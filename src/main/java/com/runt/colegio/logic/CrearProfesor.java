/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.logic;

import com.runt.colegio.dao.ProfesorDAO;
import com.runt.colegio.dto.MessageDTO;
import com.runt.colegio.dto.ProfesorDTO;
import com.runt.colegio.entities.Profesor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class CrearProfesor {
    private static final Logger LOGGER = Logger.getLogger(CrearProfesor.class.getName());
    @EJB
    private ProfesorDAO dao;
    @Resource
    protected EJBContext context;
    
    public Response crearProfesor(ProfesorDTO inDTO){
        MessageDTO msnDTO =new MessageDTO();
        msnDTO.setCode("ERROR");        
        try {
            UserTransaction ut = context.getUserTransaction();
            Profesor dataProfesor =  new Profesor();
            dataProfesor.setNombre(inDTO.getNombre());
            ut.begin();
            if(dao.create(dataProfesor)){
                msnDTO.setCode("OK");
                msnDTO.setMensaje("Creado");
                msnDTO.setObject(dataProfesor);
                ut.commit();
            }else{
                msnDTO.setCode("ERROR");
                msnDTO.setMensaje("Fallido");
                ut.rollback();
            }
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException e) {
            LOGGER.log(Level.SEVERE, e.getLocalizedMessage());
            msnDTO.setCode("ERROR");
            msnDTO.setMensaje("Fallido: "+e.getMessage());     
            msnDTO.setObject(null);
        }
        return Response.ok(msnDTO).build();
    }
    
}
