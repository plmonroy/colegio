/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.logic;

import com.runt.colegio.dao.ColegioDAO;
import com.runt.colegio.dto.ColegioDTO;
import com.runt.colegio.dto.MessageDTO;
import com.runt.colegio.entities.Colegio;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro
 */
@Stateless
public class ConsultarColegio {
    @EJB
    private ColegioDAO dao;
    public Response consultarColegio(ColegioDTO inDTO){
        MessageDTO msnDTO =new MessageDTO();
        msnDTO.setCode("ERROR"); 
        if(inDTO.getIdColegio()==null )
           inDTO.setIdColegio(1);
        //if(inDTO.getIdColegio()!=null){
        Colegio dataColegio = dao.consultaByID(inDTO.getIdColegio());
        if (dataColegio!=null){
            ColegioDTO outDTO = new ColegioDTO();
            outDTO.setNombre(dataColegio.getNombre());
            outDTO.setIdColegio(dataColegio.getIdColegio());
            msnDTO.setCode("OK");
            msnDTO.setObject(outDTO);
        }
        //}
        return Response.ok(msnDTO).build();
    }
    
}
