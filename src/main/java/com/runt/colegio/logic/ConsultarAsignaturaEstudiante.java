/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.logic;

import com.runt.colegio.dao.AsignaturaEstudianteDAO;
import com.runt.colegio.dto.AsignaturaEstudianteDTO;
import com.runt.colegio.dto.MessageDTO;
import com.runt.colegio.entities.AsignaturaEstudiante;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro
 */
@Stateless
public class ConsultarAsignaturaEstudiante {
    @EJB
    private AsignaturaEstudianteDAO dao;    
    
    public Response consultarListaAE(AsignaturaEstudianteDTO inDTO){
        MessageDTO msnDTO =new MessageDTO();
        msnDTO.setCode("ERROR"); 
        List<AsignaturaEstudiante> listAsignaturaEstudiante;
        
        if(inDTO.getGrado()!=null && inDTO.getSalon()!=null && inDTO.getNombreAsignatura()!=null){
            listAsignaturaEstudiante = dao.consultaByAsignaturaCurso(inDTO.getGrado(), inDTO.getSalon(), inDTO.getNombreAsignatura());
            if(listAsignaturaEstudiante != null && !listAsignaturaEstudiante.isEmpty()){
                List<AsignaturaEstudianteDTO> listAsignaturaEstudianteDTO = new ArrayList<>();
                for (AsignaturaEstudiante p : listAsignaturaEstudiante) {
                    AsignaturaEstudianteDTO outDTO = new AsignaturaEstudianteDTO();
                    outDTO.setAeID(null);
                    outDTO.setGrado(p.getAsignaturaEstudianteID().getGrado());
                    outDTO.setNombreAsignatura(p.getAsignaturaEstudianteID().getNombreAsignatura());
                    outDTO.setNombreEstudiante(p.getAsignaturaEstudianteID().getNombreEstudiante());
                    outDTO.setSalon(p.getAsignaturaEstudianteID().getSalon());
                    listAsignaturaEstudianteDTO.add(outDTO);
                }
                msnDTO.setCode("OK"); 
                msnDTO.setObject(listAsignaturaEstudianteDTO); 
            }
        }
        return Response.ok(msnDTO).build();

    }    
}
