/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.dto;

import com.runt.colegio.entities.AsignaturaEstudianteID;

/**
 *
 * @author Pedro
 */
public class AsignaturaEstudianteDTO {
    private AsignaturaEstudianteID aeID;
    private String nombreAsignatura;    
    private Integer grado;
    private String salon;
    private String nombreEstudiante;

    public AsignaturaEstudianteID getAeID() {
        return aeID;
    }

    public void setAeID(AsignaturaEstudianteID aeID) {
        this.aeID = aeID;
    }

    public String getNombreAsignatura() {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura) {
        this.nombreAsignatura = nombreAsignatura;
    }

    public Integer getGrado() {
        return grado;
    }

    public void setGrado(Integer grado) {
        this.grado = grado;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public String getNombreEstudiante() {
        return nombreEstudiante;
    }

    public void setNombreEstudiante(String nombreEstudiante) {
        this.nombreEstudiante = nombreEstudiante;
    }
    
    
    
    
}
