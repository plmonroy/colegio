/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.dto;

import com.runt.colegio.entities.Asignatura;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Pedro
 */
public class ProfesorDTO {
    private String nombre;
    private Set<Asignatura> listAsignaturas;
    private Boolean consultaTodo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Asignatura> getListAsignaturas() {
        return listAsignaturas;
    }

    public void setListAsignaturas(Set<Asignatura> listAsignaturas) {
        this.listAsignaturas = listAsignaturas;
    }

    public Boolean isConsultaTodo() {
        return consultaTodo;
    }

    public void setConsultaTodo(Boolean consultaTodo) {
        this.consultaTodo = consultaTodo;
    }
    
}
