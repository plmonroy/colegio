/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.services;

import com.runt.colegio.dto.ColegioDTO;
import com.runt.colegio.logic.ConsultarColegio;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro
 */
@Stateless
@Path("colegio/")
public class ColegioServicio {
    @EJB
    private ConsultarColegio consultarColegio;
    
    @POST
    @Path("consulta")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON+";charset=utf-8")
    public Response qProfesor(ColegioDTO dto){
        return consultarColegio.consultarColegio(dto);
    }    
    
}
