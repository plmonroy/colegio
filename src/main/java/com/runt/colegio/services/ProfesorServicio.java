/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.services;

import com.runt.colegio.dto.ProfesorDTO;
import com.runt.colegio.logic.ConsultarProfesor;
import com.runt.colegio.logic.CrearProfesor;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro
 */
@Stateless
@Path("profesor/")
public class ProfesorServicio {
    @EJB
    private ConsultarProfesor consultarProfesor;
    @EJB
    private CrearProfesor crearProfesor;
    
    @POST
    @Path("consulta")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON+";charset=utf-8")
    public Response qProfesor(ProfesorDTO dto){
        return consultarProfesor.consultarProfesor(dto);
    }
    
    @POST
    @Path("crear")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON+";charset=utf-8")
    public Response creaProfesor(ProfesorDTO dto){
        return crearProfesor.crearProfesor(dto);
    }    
    
    
}
