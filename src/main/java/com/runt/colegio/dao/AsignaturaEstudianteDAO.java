/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.dao;

import com.runt.colegio.entities.AsignaturaEstudiante;
import com.runt.colegio.util.GeneralDAO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;

/**
 *
 * @author Pedro
 */
@Stateless
public class AsignaturaEstudianteDAO extends GeneralDAO<AsignaturaEstudiante>{
    public AsignaturaEstudianteDAO(){
        super(AsignaturaEstudiante.class);
    }
    
    public List<AsignaturaEstudiante> consultaByAsignaturaCurso(Integer grado,String salon, String nombreAsig ){
        Map<String, Object> params = new HashMap<>();
        params.put("grado", grado);
        params.put("salon", salon);
        params.put("nombreAsig", nombreAsig);  
        return this.filtrar("AsignaturaEstudiante.findEstudiantesAsignatura", params);
    }
            
    
    
}
