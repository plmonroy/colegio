/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.dao;

import com.runt.colegio.entities.Profesor;
import com.runt.colegio.util.GeneralDAO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;

/**
 *
 * @author Pedro
 */
@Stateless
public class ProfesorDAO extends GeneralDAO<Profesor>{
    
    public ProfesorDAO(){
        super(Profesor.class);
    }
    
    public Profesor consultaByID(final String id){
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        return this.filtrarUnicoRegistro("Profesor.findById", params);
    }
    
    public List<Profesor> consultaTodos(){
        return this.listarAll("Profesor.findAll");
    }    
    
}
