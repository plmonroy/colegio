/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.dao;

import com.runt.colegio.entities.Colegio;
import com.runt.colegio.util.GeneralDAO;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateless;

/**
 *
 * @author Pedro
 */
@Stateless
public class ColegioDAO extends GeneralDAO<Colegio>{
    public ColegioDAO(){
        super(Colegio.class);
    }
    
    public Colegio consultaByID (final Integer id){
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        return this.filtrarUnicoRegistro("Colegio.findById", params);        
    }
    
}
