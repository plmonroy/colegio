package com.runt.colegio.test;

import java.util.ArrayList;
import java.util.List;

import com.runt.colegio.entities.Asignatura;
import com.runt.colegio.entities.AsignaturaID;
import com.runt.colegio.entities.Curso;
import com.runt.colegio.entities.CursoID;
//import com.runt.colegio.entities.Colegio;
//import com.runt.colegio.entities.Curso;
//import com.runt.colegio.entities.CursoID;
import com.runt.colegio.entities.Profesor;

public class TestCode {

	public static void main(String[] args) {
		Curso curso1 = new Curso();
		Curso curso2 = new Curso();
		CursoID ci_1 = new CursoID((Integer) 2, "B");
		CursoID ci_2 = new CursoID((Integer) 2, "C");
//		Colegio colegio = Colegio.getInstancia();;
//		Colegio colegio2 = null;
//
//		
//		System.out.println("Same HC: "+(ci_1.hashCode()==ci_2.hashCode()));
//		System.out.println("Equals?: "+(ci_1.equals(ci_2)));
//		
//		curso1.setCursoID(ci_1);
//		curso2.setCursoID(ci_2);
//				
//		System.out.println("HC1: "+(curso1.hashCode()));
//		System.out.println("HC2: "+(curso2.hashCode()));
//		
//		System.out.println("Same HC: "+(curso1.hashCode()==curso2.hashCode()));
//		System.out.println("Equals?: "+(curso2.equals(curso1)));
//		System.out.println(curso1.toString());
//		
//		System.out.println("Entidad COlegio test: ============");
//		colegio.setIdColegio(123);
//		colegio.setNombre("La presentacion");
//		colegio2 = Colegio.getInstancia();
//		System.out.println("C1: "+(colegio.hashCode()));
//		System.out.println("C2: "+(colegio2.hashCode()));
//		System.out.println("Equals?: "+(colegio.equals(colegio2)));		
//		
//		
//	
//		
		Asignatura as1 = new Asignatura();
		//as1.setAsignaturaID(new AsignaturaID("Math", curso1));
				
		Asignatura as2 = new Asignatura();
		//as2.setAsignaturaID(new AsignaturaID("Math", curso2));

		Asignatura as3 = new Asignatura();
		//as3.setAsignaturaID(new AsignaturaID("Español", curso1));

		List<Asignatura> lista1 = new ArrayList<Asignatura>();
		lista1.add(as1);
		lista1.add(as2);
		List<Asignatura> lista2 = new ArrayList<Asignatura>();
		lista2.add(as1);
		lista2.add(as3);
		List<Asignatura> lista3 = new ArrayList<Asignatura>();
		lista3.add(as2);
		lista3.add(as3);	
		List<Asignatura> lista4 = new ArrayList<Asignatura>();
		lista4.add(as1);		
		lista4.add(as2);
		lista4.add(as3);			
		
		Profesor pf1 = new Profesor();
		Profesor pf2 = new Profesor();
		pf1.setNombre("Emilio");
		pf2.setNombre("Emilio");
		
		//pf1.setListAsignaturas(listAsignaturas);
		//pf1.setListAsignaturas(lista1);
		//pf2.setListAsignaturas(lista2);
		
		
		System.out.println("HC1: "+(pf1.hashCode()));
		System.out.println("HC2: "+(pf2.hashCode()));		
		System.out.println("Equals?: "+(pf1.equals(pf2)));
		
		
		
		

		
	}

}
