/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.entities;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author Pedro
 */
@Entity
@NamedQueries({
    @NamedQuery (name = "AsignaturaEstudiante.findEstudiantesAsignatura",query = "SELECT r FROM AsignaturaEstudiante r where r.asignaturaEstudianteID.grado = :grado and r.asignaturaEstudianteID.salon = :salon and r.asignaturaEstudianteID.nombreAsignatura =:nombreAsig " )
})

public class AsignaturaEstudiante implements Serializable{
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private AsignaturaEstudianteID asignaturaEstudianteID;
    
    @ManyToOne(optional = true)
    @JoinColumns({
        @JoinColumn(name = "nombreAsignatura",referencedColumnName = "nombre", insertable = false, updatable = false),
        @JoinColumn(name = "grado",referencedColumnName = "grado", insertable = false, updatable = false),
        @JoinColumn(name = "salon",referencedColumnName = "salon", insertable = false, updatable = false)
    })
    private Asignatura asignatura;    

    @ManyToOne
    @JoinColumn(name="nombreEstudiante",referencedColumnName = "nombre",unique = false,insertable = false, updatable = false) 
    private Estudiante estudiante;

    
    /*@OneToMany(fetch = FetchType.EAGER,targetEntity = Asignatura.class, mappedBy = "asignaturaEstudiante")
    private Set<Asignatura> listAsignatura;
    
    @OneToMany(fetch = FetchType.EAGER,targetEntity = Estudiante.class, mappedBy = "asignaturaEstudiante")
    private Set<Estudiante> listEstudiante;    
    */
    public AsignaturaEstudianteID getAsignaturaEstudianteID() {
        return asignaturaEstudianteID;
    }

    public void setAsignaturaEstudianteID(AsignaturaEstudianteID asignaturaEstudianteID) {
        this.asignaturaEstudianteID = asignaturaEstudianteID;
    }


    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.asignaturaEstudianteID);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AsignaturaEstudiante)) {
            return false;
        }

        AsignaturaEstudiante that = (AsignaturaEstudiante) obj;
        return Objects.equals(getAsignaturaEstudianteID(),that.getAsignaturaEstudianteID());
    }
    
    
    
    
}
