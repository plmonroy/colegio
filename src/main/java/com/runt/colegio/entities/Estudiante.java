package com.runt.colegio.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Estudiante implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column
    private String nombre;

    @OneToMany(fetch = FetchType.EAGER, targetEntity = AsignaturaEstudiante.class, mappedBy = "estudiante")
    private Set<AsignaturaEstudiante> listAsignaturaEstudiante;
    //private Curso curso;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<AsignaturaEstudiante> getListAsignaturaEstudiante() {
        return listAsignaturaEstudiante;
    }

    public void setListAsignaturaEstudiante(Set<AsignaturaEstudiante> listAsignaturaEstudiante) {
        this.listAsignaturaEstudiante = listAsignaturaEstudiante;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Estudiante)) {
            return false;
        }
        Estudiante that = (Estudiante) obj;
        return that.getNombre().equals(this.getNombre());
    }

}
