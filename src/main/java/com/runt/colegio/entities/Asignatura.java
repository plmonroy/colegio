package com.runt.colegio.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Asignatura implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private AsignaturaID asignaturaID;

    @ManyToOne(optional = true)
    @JoinColumn(unique = false, nullable = true)
    private Profesor profesor;


    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "grado", insertable = false, updatable = false),
        @JoinColumn(name = "salon", insertable = false, updatable = false)
    })
    private Curso curso;
    /* //descomentar para consultar las 3 entidades en el solo servicio de profesor.
    @OneToMany(fetch = FetchType.EAGER, targetEntity = AsignaturaEstudiante.class, mappedBy = "asignatura")
    private Set<AsignaturaEstudiante> listAsignaturaEstudiante;
    */

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public AsignaturaID getAsignaturaID() {
        return asignaturaID;
    }

    public void setAsignaturaID(AsignaturaID asignaturaID) {
        this.asignaturaID = asignaturaID;
    }

    /*public Set<AsignaturaEstudiante> getListAsignaturaEstudiante() {
        return listAsignaturaEstudiante;
    }

    public void setListAsignaturaEstudiante(Set<AsignaturaEstudiante> listAsignaturaEstudiante) {
        this.listAsignaturaEstudiante = listAsignaturaEstudiante;
    }*/

    @Override
    public String toString() {
        return asignaturaID.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((asignaturaID == null) ? 0 : asignaturaID.hashCode());
        //result = prime * result + ((listEstudiantes == null) ? 0 : listEstudiantes.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Asignatura)) {
            return false;
        }
        Asignatura that = (Asignatura) o;
        return Objects.equals(getAsignaturaID(), that.getAsignaturaID());
    }

}
