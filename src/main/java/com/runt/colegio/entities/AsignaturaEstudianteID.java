/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runt.colegio.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Pedro
 */
@Embeddable
public class AsignaturaEstudianteID implements Serializable{
    private static final long serialVersionUID = 1L;
    @Column
    private String nombreAsignatura;    
    
    @Column
    private Integer grado;
    
    @Column
    private String salon;
    
    @Column
    private String nombreEstudiante;

    public AsignaturaEstudianteID() {
    }

    public AsignaturaEstudianteID(String nombreAsignatura, Integer grado, String salon, String nombreEstudiante) {
        this.nombreAsignatura = nombreAsignatura;
        this.grado = grado;
        this.salon = salon;
        this.nombreEstudiante = nombreEstudiante;
    }

    public String getNombreAsignatura() {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura) {
        this.nombreAsignatura = nombreAsignatura;
    }

    public Integer getGrado() {
        return grado;
    }

    public void setGrado(Integer grado) {
        this.grado = grado;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public String getNombreEstudiante() {
        return nombreEstudiante;
    }

    public void setNombreEstudiante(String nombreEstudiante) {
        this.nombreEstudiante = nombreEstudiante;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.nombreAsignatura);
        hash = 47 * hash + Objects.hashCode(this.grado);
        hash = 47 * hash + Objects.hashCode(this.salon);
        hash = 47 * hash + Objects.hashCode(this.nombreEstudiante);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AsignaturaEstudianteID)) {
            return false;
        }
        final AsignaturaEstudianteID that = (AsignaturaEstudianteID) obj;
        return Objects.equals(this.nombreAsignatura, that.nombreAsignatura) && 
                Objects.equals(this.salon, that.salon) && 
                Objects.equals(this.grado, that.grado) && 
                Objects.equals(this.nombreEstudiante, that.nombreEstudiante);

    }
    
    
    
    
    
}
