package com.runt.colegio.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CURSO")
public class Curso implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private CursoID cursoID;
    
    @ManyToOne(optional = false)    
    @JoinColumn(unique = false,nullable=false)
    private Colegio colegio;

    @OneToMany(fetch = FetchType.EAGER,targetEntity = Asignatura.class, mappedBy = "curso")
    private List<Asignatura> listAsignaturas;

    public List<Asignatura> getListAsignaturas() {
        return listAsignaturas;
    }

    public void setListAsignaturas(List<Asignatura> listAsignaturas) {
        this.listAsignaturas = listAsignaturas;
    }

    public CursoID getCursoID() {
        return cursoID;
    }

    public void setCursoID(CursoID cursoID) {
        this.cursoID = cursoID;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cursoID == null) ? 0 : cursoID.hashCode());
        result = prime * result + ((listAsignaturas == null) ? 0 : listAsignaturas.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Curso)) {
            return false;
        }
        Curso that = (Curso) o;
        return Objects.equals(getCursoID(), that.getCursoID());
        //return Objects.equals(getCursoID().getGrado(), that.getCursoID().getGrado()) 
        //		&& Objects.equals(getCursoID().getSalon(), that.getCursoID().getSalon());
    }

    @Override
    public String toString() {
        return cursoID.toString();
    }

}
