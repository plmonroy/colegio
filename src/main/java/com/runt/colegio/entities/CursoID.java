package com.runt.colegio.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;

import javax.persistence.Embeddable;

@Embeddable
public class CursoID implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column
    private Integer grado;
    @Column
    private String salon;

    public CursoID() {
    }

    public CursoID(Integer grado, String salon) {
        //super();
        this.grado = grado;
        this.salon = salon;
    }

    @Override
    public String toString() {
        return grado + salon;
        //return "CursoID [grado=" + grado + ", salon=" + salon + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CursoID)) {
            return false;
        }
        CursoID that = (CursoID) o;
        //return (getGrado() == that.getGrado()) && (getSalon() == that.getSalon());
        return Objects.equals(getGrado(), that.getGrado()) && Objects.equals(getSalon(), that.getSalon());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((grado == null) ? 0 : grado.hashCode());
        result = prime * result + ((salon == null) ? 0 : salon.hashCode());
        return result;
    }

    public Integer getGrado() {
        return grado;
    }

    public void setGrado(Integer grado) {
        this.grado = grado;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

}
