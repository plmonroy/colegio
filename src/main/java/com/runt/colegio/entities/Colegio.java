package com.runt.colegio.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@NamedQueries({
    @NamedQuery (name = "Colegio.findById",query = "SELECT r FROM Colegio r where r.idColegio = :id" )
})

@Table(name = "COLEGIO")
public class Colegio implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Colegio INSTANCIA;

    private Colegio() {
    }

    static {
        try {
            INSTANCIA = new Colegio();
        } catch (Exception e) {
            throw new RuntimeException("Exception occured in creating singleton instance");
        }
    }

    @Id
    private Integer idColegio;

    private String nombre;

    @OneToMany(fetch = FetchType.EAGER,targetEntity = Curso.class, mappedBy = "colegio")
    private List<Curso> listCursos;

    public Integer getIdColegio() {
        return idColegio;
    }

    public void setIdColegio(Integer idColegio) {
        this.idColegio = idColegio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Curso> getListCursos() {
        return listCursos;
    }

    public void setListCursos(List<Curso> listCursos) {
        this.listCursos = listCursos;
    }

    public static Colegio getInstancia() {
        return INSTANCIA;
    }

}
