package com.runt.colegio.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({
    @NamedQuery (name = "Profesor.findById",query = "SELECT r FROM Profesor r where r.nombre = :id" ),
    @NamedQuery (name = "Profesor.findAll",query = "SELECT r FROM Profesor r" )
})
public class Profesor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column
    private String nombre;

    @OneToMany(fetch = FetchType.EAGER,targetEntity = Asignatura.class, mappedBy = "profesor")
    private Set<Asignatura> listAsignaturas;

    //================================		
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Asignatura> getListAsignaturas() {
        return listAsignaturas;
    }

    public void setListAsignaturas(Set<Asignatura> listAsignaturas) {
        this.listAsignaturas = listAsignaturas;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        //result = prime * result + ((listAsignaturas == null) ? 0 : listAsignaturas.hashCode());
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Profesor)) {
            return false;
        }
        Profesor that = (Profesor) obj;
        return getNombre().equals(that.getNombre());
    }

}
