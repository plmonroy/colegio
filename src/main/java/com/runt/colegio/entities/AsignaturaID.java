package com.runt.colegio.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;

import javax.persistence.Embeddable;

@Embeddable
public class AsignaturaID implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column
    private String nombre;    
    
    @Column
    private Integer grado;
    
    @Column
    private String salon;

    //private Curso curso;
    /*
    @Column
    private Integer grado;
    @Column
    private String salon;    
    */

    public AsignaturaID() {
    }

    public AsignaturaID(String nombre,Integer grado, String salon) {
        this.nombre = nombre;
        this.grado = grado;
        this.salon = salon;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getGrado() {
        return grado;
    }

    public void setGrado(Integer grado) {
        this.grado = grado;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.nombre);
        hash = 97 * hash + Objects.hashCode(this.grado);
        hash = 97 * hash + Objects.hashCode(this.salon);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AsignaturaID)) {
            return false;
        }
        AsignaturaID that = (AsignaturaID) obj;
        return Objects.equals(nombre, that.getNombre()) && Objects.equals(grado, that.getGrado()) && Objects.equals(salon, that.getSalon());
    }

    @Override
    public String toString() {
        return nombre + " - [" + grado + salon + "]";
    }

}
