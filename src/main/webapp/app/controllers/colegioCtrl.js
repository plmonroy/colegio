angular.module("colegioApp", []).controller("colegioCtrl", function($scope,$http) {  
    $scope.message="Hello World";
    $scope.ulrbase = 'webresources/';
    $scope.showListaProfes = false;
    $scope.showAsignaturas = false;
    $scope.showEstudiantes = false;
    
    $scope.datos = {};
    $scope.datosResultado = [];
    $scope.datosListaAsigProf = [];
    $scope.datosEstudiantes = [];
    
    
    $scope.selectedProfesor = null;
    $scope.nombreColegio = null;    
    //funciones
    $scope.listarProfesores = listarProfesores;
    $scope.changeProfesor = changeProfesor;
    $scope.resetVariables = resetVariables;
    $scope.verEstudiantes = verEstudiantes;
    
    $scope.datos.idColegio=1;
    $http.post($scope.ulrbase + 'colegio/consulta',$scope.datos)
        .then(function onSuccess(response){
                if (response.data.code === 'OK') {
                    $scope.datosResultado = response.data.object;
                    if ($scope.datosResultado.length !== 0) {
                        $scope.nombreColegio = $scope.datosResultado.nombre;
                       }                
                }
               },function onError(response){
                alert('Error:' ,response.data.mensaje);    
        });    
     
    function resetVariables(){
        $scope.showAsignaturas = false;
        $scope.showEstudiantes = false;
    };
    
    
    
    function listarProfesores() {
        resetVariables();
        $scope.datos = {};
        $scope.datos.consultaTodo = true;
        $http.post($scope.ulrbase + 'profesor/consulta',$scope.datos)
            .then(function onSuccess(response){
                    if (response.data.code === 'OK') {
                        $scope.datosResultado = response.data.object;
                        if ($scope.datosResultado.length !== 0) {
                            $scope.showListaProfes = true;
                            console.log($scope.datosResultado);   
                           }                
                    }
                   },function onError(response){
                    alert('Error:' ,response.data.mensaje);    
            });

    };
    function changeProfesor() {
        $scope.showAsignaturas = true;
        $scope.datosListaAsigProf = $scope.datosResultado.find( x => x.nombre===$scope.selectedProfesor).listAsignaturas;
        //listar Asignaturas profesor seleccionado
    };
     function verEstudiantes(x){    

        $scope.datos = {};
        $scope.datos.nombreAsignatura = x.asignaturaID.nombre;
        $scope.datos.grado = x.asignaturaID.grado;
        $scope.datos.salon = x.asignaturaID.salon;
        
        $http.post($scope.ulrbase + 'asignaturaestudiante/consulta',$scope.datos)
            .then(function onSuccess(response){
                    if (response.data.code === 'OK') {
                        $scope.datosEstudiantes = response.data.object;
                        $("#modalEstudiantes").modal('show');
                        if ($scope.datosEstudiantes.length !== 0) {
                            console.log($scope.datosEstudiantes);   
                           }                
                    }
                   },function onError(response){
                    alert('Error:' ,response.data.mensaje);    
            });         
         
     }
    
    
});